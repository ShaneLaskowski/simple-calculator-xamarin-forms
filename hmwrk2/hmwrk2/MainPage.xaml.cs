﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace hmwrk2
{
    public class Constants
    {
        //the "const" keyword isn't used because it would require the constant values assigned to be "compile-time constants".
        //the assigned value on right of = isn't known at compile time, it must be evaluated at run-time. Arrays seem to need to be evaluated at run-time.
        //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/const
        //https://www.c-sharpcorner.com/article/runtime-and-compiletime-constants-in-c-sharp/
        //thus the "readonly" modifier is used.


        public static readonly char[] delimiterChars = { ' ', ',', '.', ':', '\t' }; //this delimiter may not be useful.

        //this delimiter is used to extract the operands (math symbols) out of the math expression
        public static readonly string[] delimiterOperands = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-0",
                                                             "-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", " ", "."};
        
        //this delimiter is used to extract the operators (numbers) from the math expression.
        public static readonly string[] delimiterOperators = { "+", " - ", "*", "/", " " };
    }

    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        private void OverWriteEmptyExpression()
        {
            if (CalculationDisplay.Text == "0")
                CalculationDisplay.Text = "";
        }
        void Button_Number_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            OverWriteExpressionHavingSciNotation();
            OverWriteEmptyExpression(); //removes 0 from label if its the only character of the display
            Button btn = (Button)sender;
            CalculationDisplay.Text += btn.Text;
            //OuterLayoutGrid.RowDefinitions[1].Height = e.NewValue;
        }
        void Button_Operator_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            OverWriteExpressionHavingSciNotation();
            //OverWriteEmptyExpression();
            Button btn = (Button)sender;
            CalculationDisplay.Text += " " + btn.Text + " ";
        }
        void Button_Clear_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            CalculationDisplay.Text = "0";// make this "" ? **********************
        }
        private void Button_Decimal_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            OverWriteExpressionHavingSciNotation();
            //OverWriteEmptyExpression();
            Button btn = (Button)sender;
            CalculationDisplay.Text += btn.Text;
        }
        private void Button_Delete_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            if (CalculationDisplay.Text.Length > 1)
            {
                if (CalculationDisplay.Text.Length >= 3 && CalculationDisplay.Text[CalculationDisplay.Text.Length - 1] == ' ' &&
                         CalculationDisplay.Text[CalculationDisplay.Text.Length - 3] == ' ')
                    CalculationDisplay.Text = CalculationDisplay.Text.Remove(CalculationDisplay.Text.Length - 3, 3);
                else CalculationDisplay.Text = ((CalculationDisplay.Text).Remove(CalculationDisplay.Text.Length - 1));

                //This lets it so that a leading operand can be deleted within 1 button press
                //*****REDO this so that it deletes 
                //if (CalculationDisplay.Text.Length > 1 && CalculationDisplay.Text[CalculationDisplay.Text.Length - 1] == ' ')
                //  CalculationDisplay.Text = ((CalculationDisplay.Text).Remove(CalculationDisplay.Text.Length - 1));

            }
            else CalculationDisplay.Text = "0";

        }

        /// <summary>
        ///     add negative sign to right most number or remove the negative sign if it has one.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Sign_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            OverWriteExpressionHavingSciNotation();
            string mathExpression = CalculationDisplay.Text;
            int expressionLength = mathExpression.Length;
            int beginningIndexOfLastNumber = mathExpression.LastIndexOf(' ');

            if(beginningIndexOfLastNumber == -1 && expressionLength == 0)
            {
                //displayed math expression is empty
                //insert '-' at front of string
                CalculationDisplay.Text = '-' + CalculationDisplay.Text;
            }
            else if (beginningIndexOfLastNumber == -1 && expressionLength > 0)
            {
                //displayed math expression contains one number, a sign, or an operator
                
                if (CalculationDisplay.Text[0] == '-')
                    CalculationDisplay.Text = CalculationDisplay.Text.Remove(0, 1);
                else CalculationDisplay.Text = '-' + CalculationDisplay.Text;
            }
            else
            {
                //there is a space ==> beginningIndexOfLastNumber is at front of where a number is/would be.

                //check next index, if its a '-', remove it
                if (mathExpression.Length - 1 == beginningIndexOfLastNumber)
                {
                    CalculationDisplay.Text += "-";
                    //or [example: "2 + " becomes "2 + -"]
                    //***********************HANDLE THIS !!!!
                }
                else if (mathExpression[beginningIndexOfLastNumber + 1] == '-')
                {
                    CalculationDisplay.Text = CalculationDisplay.Text.Remove(beginningIndexOfLastNumber + 1, 1);
                }
                else
                {
                    //if its not, replace the ' ' with a " -".  [example: "2 + 3 - 5" becomes "2 + 3 - -5"]
                    CalculationDisplay.Text = CalculationDisplay.Text.Insert(beginningIndexOfLastNumber + 1, "-");

                }
            }

        }


        /// <summary>
        ///     Takes in user input from the displayed label, calculates it, and then displays the result on the same label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Button_Equal_Clicked(object sender, EventArgs e)
        {
            VibratePhone();
            OverWriteExpressionHavingSciNotation();
            string userMathFormula = CalculationDisplay.Text;
            int inputLength = userMathFormula.Length;
            //perhaps change string[] to char[] ????
            string[] ExpressionOperands = userMathFormula.Split(Constants.delimiterOperators, StringSplitOptions.RemoveEmptyEntries);
            string[] ExpressionOperators = userMathFormula.Split(Constants.delimiterOperands, StringSplitOptions.RemoveEmptyEntries);
            Stack<float> operandStack = new Stack<float>(); //holds the operands/numbers that the user inputted
            Stack<char> operatorStack = new Stack<char>(); //holds the operators (math signs) that the user inputted
            string result = "0";

            try
            {
                //InitializeStackRightToLeft<float>(ExpressionOperands, operandStack); //this is a generic that preforms the 2 code sections below, but doesn't work.
                for (int i = ExpressionOperators.Length - 1; i >= 0; i--)
                    operatorStack.Push(char.Parse(ExpressionOperators[i]));

                for (int i = ExpressionOperands.Length - 1; i >= 0; i--)
                {
                    if (!HasNegativeSignWithoutValue(ExpressionOperands[i]) && !HasTooManyDecimals(ExpressionOperands[i]))
                    {
                        operandStack.Push(float.Parse(ExpressionOperands[i])); //****ensure there are no operand elements with '-' but without a value (use a function to check)
                    }
                }
                //result is displayed to user
                result = CalculateMathExpression(operatorStack, operandStack);
            }
            catch(Exception ex)
            {
                DisplayAlert("Error", "Something Bad Happened", "Okay");
            }
            CalculationDisplay.Text = result;
        }
        
        private string CalculateMathExpression(Stack<char> operatorStack, Stack<float> operandStack)
        {

            //when all that is left is a single operand, a single number, that operand is our expression's result.
            //going to need to do a bunch of bad input checks/handling: users love to divie by 0.
            //************ensure that negative sign in 1st number is not counted as firstLeadingOperator.
            char firstLeadingOperator;
            char secondLeadingOperator;
            float firstLeadingOperand;
            float secondLeadingOperand;
            float thirdLeadingOperand;
            string result;

            if (operandStack.Count != 0)
                result = operandStack.Peek().ToString();
            else result = "";

            if (operatorStack.Count + 1 != operandStack.Count && CalculationDisplay.Text != "-0")
            {
                DisplayAlert("Error", "Invalid math expression format", "OK");
                return "0";
            }
            while (operatorStack.Count > 0 && operandStack.Count != 1)
            {
                //pop off the 1st operator, then the second operator (if there is more than 1 left)
                if(operatorStack.Count > 1)
                {
                    firstLeadingOperator = operatorStack.Pop();
                    secondLeadingOperator = operatorStack.Pop();

                    //check if the 1st operator has greater or equal precedence of the 2nd operator
                    if (CompareOperatorPrecedence(firstLeadingOperator, secondLeadingOperator))
                    {
                        operatorStack.Push(secondLeadingOperator);//push back the 2nd operator

                        //pop off the top 2 operands
                        firstLeadingOperand = operandStack.Pop();
                        secondLeadingOperand = operandStack.Pop();

                        //execute math operation with the operator and the two operands.
                        //and then push that result back on top of the operand stack
                        if (IsDivideByZero(firstLeadingOperand, secondLeadingOperand, firstLeadingOperator))
                        {
                            DisplayAlert("Error", "Divide by 0", "OK");
                            return "0";
                        }
                        else operandStack.Push(MathExecution(firstLeadingOperand, secondLeadingOperand, firstLeadingOperator));
                    }
                    else //need to preform the operation with higher precedence
                    {
                        //if not, push the 1st operator back on the stack, then pop of the top 3 operands off the stack
                        operatorStack.Push(firstLeadingOperator);
                        firstLeadingOperand = operandStack.Pop();
                        secondLeadingOperand = operandStack.Pop();
                        thirdLeadingOperand = operandStack.Pop();

                        //perform the math operation on the bottom 2 operands with the 2nd operator that has higher precedence,
                        //then push that result on the operand stack
                        if (IsDivideByZero(secondLeadingOperand, thirdLeadingOperand, secondLeadingOperator))
                        {
                            DisplayAlert("Error", "Divide by 0", "OK");
                            return "0";
                        }
                        else operandStack.Push(MathExecution(secondLeadingOperand, thirdLeadingOperand, secondLeadingOperator));

                        //then push that 1st operand back onto the operand stack as well.
                        operandStack.Push(firstLeadingOperand);
                    }
                    //repeat process until all that is left between the two stacks is a single operand.
                    //ensure that all types off bad input are handled, perhaps perform this before invoking this algorithm, atleast some.
                }
                //in this case only 2 operands and 1 operator should be left in the stacks.
                else 
                {
                    firstLeadingOperator = operatorStack.Pop();
                    firstLeadingOperand = operandStack.Pop();
                    secondLeadingOperand = operandStack.Pop();


                    if (IsDivideByZero(firstLeadingOperand, secondLeadingOperand, firstLeadingOperator))
                    {
                        DisplayAlert("Error", "Divide by 0", "OK");
                        return "0";
                    }
                    else result = (MathExecution(firstLeadingOperand, secondLeadingOperand, firstLeadingOperator)).ToString();
                    //do stuff
                }

            }

            return result;
        }

        private float MathExecution(float firstOperand, float secondOperand, char Operator)
        {
            //Make this a Switch statement (C# allows use of strings for case values, not just int)
            if (Operator == '+')
                return firstOperand + secondOperand;
            else if (Operator == '-')
                return firstOperand - secondOperand;
            else if (Operator == '*')
                return firstOperand * secondOperand;
            else if (Operator == '/')
                return firstOperand / secondOperand;
            else return 0.0f;
            //toss exception for else condtion?

        }

        /// <summary>
        ///     compares the 1st operator's precedence with the second operator's precedence.
        /// </summary>
        /// <param name="firstOperator"></param>
        /// <param name="secondOperator"></param>
        /// <returns>eturns true if 1st operator is equal or greater precedence than the second operator, else returns false </returns>
        private bool CompareOperatorPrecedence(char firstOperator, char secondOperator)
        {
            int precedenceValueFirstOperator = GetOperatorPrecedenceValue(firstOperator);
            int precedenceValueSecondOperator = GetOperatorPrecedenceValue(secondOperator);

            return (precedenceValueFirstOperator >= precedenceValueSecondOperator ? true : false);
        }

        /// <summary>
        ///     checks the operators precedence value.
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        private int GetOperatorPrecedenceValue(char op)
        {
            if (op == '+' || op == '-')
                return 1;
            else if (op == '*' || op == '/')
                return 2;
            else
                return -1; //prehaps throw an exception instead?
        }

        private bool IsDivideByZero(float firstNum, float secondNum, char MathOperator)
        {
            if (secondNum == 0.0f && MathOperator == '/')
                return true;
            else return false;
        }
        
        //used to check if input is a number or just a '-' sign without any value attached to it
        private bool HasNegativeSignWithoutValue(string number)
        {
            if (number == "-")
                return true;
            else return false;
        }

        private void OverWriteExpressionHavingSciNotation()
        {
            if (CalculationDisplay.Text.Contains("E+"))//this will tell of if something in the expression contains scientific notation
                CalculationDisplay.Text = "0";
        }

        private void VibratePhone()
        {
            try
            {
                // Use default vibration length 500ms
                //Vibration.Vibrate();

                // Or use specified time
                //var duration = TimeSpan.FromSeconds(1);
                var duration = TimeSpan.FromMilliseconds(25);
                Vibration.Vibrate(duration);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

        private bool HasTooManyDecimals(string str)
        {
            var decimalCount = 0;
            foreach(char character in str)
            {
                if (character == '.')
                    decimalCount++;
            }
            if(decimalCount > 1)
                return true;
            else return false;
        }

        /*Need to figure out how to cast to type T without knowing what it is.
        private void InitializeStackRightToLeft<T>(string[] ExpressionComponents, Stack<T> componentContainer)
        {
            for (int i = ExpressionComponents.Length - 1; i >= 0; i--)
                componentContainer.Push((T)ExpressionComponents[i]);

        }*/
    }
}

/*
  a operation takes two operands, one on left of operator and one of the right
            //need to build up the 1st number until this loop hits a space, then take in the operator
            //then take up the 2nd number.  after this calculate.  the answer is then placed is then inserted at the front
            of the userMathFormula so that it can be used by other operations. ****ISSUE not forgetting PEMDAS !!!!!
            
            //use a doubly-Linked list to grab and store operands and operators from the userMathFormula.
            //store operands and operators unless they are either * or / ..... if * or / pop off the trailing operand
            //and perform the operation with that operand,  the operator that is * or /, and the next operand in the userMathFormula


            foreach (char c in userMathFormula)
            {
                 if(Char.IsDigit(c)) //the digit is added
                  {
                    grabbedItem += c;
                  }
                 else if(c.Equals('+') || c.Equals('-')) // the operand is added
                 {
                    grabbedItem += c;
                    calculation.AddLast(grabbedItem);
                }
                else if(c.Equals(' ')) //this should mark the end of a number (a number contains digits).
                 {
                    calculation.AddLast(grabbedItem);
                 }
                 else if(c.Equals('*') || c.Equals('/')) //instead of pushing c into the Linked-List, pop off the tail number and perform the math operation with next number in userMathFormula
                 {
                    //pop the next item (an entire number from userMathFormula
                    //pop an entire number from rear of the Linked-List
                    //perform the math operation with these 2 numbers
                    //push the answer from the operation to the rear of the Linked-List
                    //*note: avoid divide by 0
                }
                else
                 {
                    //error !!!
                 }
                 
            }
            //by now, the operands and operators have been extracted from userMathFormula
            //the Linked-List is now filled with numbers and + or - operands.... or its just empty, or its just number(s).
            //now pop from the front of the Linked-List to handle the rest of the contents.
            //***note, typically, 3 items will be removed from the LL and 1 will be put back at the front.

    */