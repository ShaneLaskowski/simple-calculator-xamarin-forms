## SimpleCalculator
* A simple Calculator with tactile feedback.  (Tested on Android). 
* * *
### SnapShot(s)
![IMAGE](https://i.imgur.com/3nt3cLxm.png)
* * *
### Issues
* Not tested on iOS (need iOS device, simulator, or MacOS computer).
* Handling overflow, should scientfic notation be used?  Should float, double, or decimal datatypes be used (consider precision)? should size of number entered be limited?  should decimal numbers be rounded to a certain accuracy?
* When user puts the app in the background, the content of the output display is shifted to the left side of the screen.
* * *
### Considerations For Improvements
* Learn and implement MVVM architectural pattern if it benefits code.
* Allow user to operate on scientific annotated numbers (instead of just immediataly deleting result after next keypress).
* Allow user to select where on the output display they wish to add/delete operands/operators.
* Implement Parathesis button (try recursive functions).
* Make the output display scrollable (left and right) so that an abitrary large expression can be typed in and remain visable.
* Add in "History" button
* Refractor code, write test cases, and add more documentation.
* * *
### Author
* **Shane Laskowski**